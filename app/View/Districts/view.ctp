<div class="districts view">
	<h2><?php echo __('DISTRITO ... Id('. $district['District']['id'].')'); ?></h2>
	<dl>
	
		<!-- 		
				<dt><?php echo __('Id'); ?></dt>
				<dd>
					<?php echo h($district['District']['id']); ?>
					&nbsp;
				</dd>
		 -->		
		 
		<dt><?php echo __('Descripción'); ?></dt>
		<dd>
			<?php echo h($district['District']['name']); ?>
			&nbsp;
		</dd>
		
	</dl>
</div>

<div class="actions">
	<h3><?php echo __('Opciones'); ?></h3>
	<h4><?php echo __('DISTRITOS'); ?></h4>
	<ul>
		<li><?php echo $this->Html->link(__('Modificar'), array('action' => 'edit', $district['District']['id'])); ?> </li>
		<li>	
			<?php echo $this->Form->postLink(	__('Borrar'), 
																			array('action' => 'delete', $district['District']['id']), 
																			null, 
																			__('¿BORRAMOS el DISTRITO < %s >?', $district['District']['name'])
																		 ); 
			?> 
		</li>
		<li><?php echo $this->Html->link(__('Listar'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Crear'), array('action' => 'add')); ?> </li><br>
	</ul>
		
	<h4><?php echo __('CIRCUITOS'); ?></h4>
	<ul>
		<li><?php echo $this->Html->link(__('Listar'), array('controller' => 'circuits', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Crear'), array('controller' => 'circuits', 'action' => 'add')); ?> </li>
	</ul>
</div>

	<div class="related">
			<br>
			<h2><?php echo __('CIRCUITOS relacionados'); ?></h2>
			<?php if (!empty($district['Circuit'])): ?>
					<table cellpadding = "0" cellspacing = "0">
						<tr>
							<th><?php echo __('Id'); ?></th>
							<th><?php echo __('Descripción'); ?></th>
							<!-- <th><?php echo __('Distrito'); ?></th> -->
							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
						<?php
							$i = 0;
							foreach ($district['Circuit'] as $circuit): ?>
							<tr>
								<td><?php echo $circuit['id']; ?></td>
								<td><?php echo $circuit['name']; ?></td>
								<!-- <td><?php echo $circuit['district_id']; ?></td> -->
								<td class="actions">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'circuits', 'action' => 'view', $circuit['id'])); ?>
									<?php echo $this->Html->link(__('Modificar'), array('controller' => 'circuits', 'action' => 'edit', $circuit['id'])); ?>
									<?php echo $this->Form->postLink(__('Borrar'), array('controller' => 'circuits', 'action' => 'delete', $circuit['id']), null, __('¿BORRAMOS el CIRCUITO < %s >?', $circuit['name'])); ?>
								</td>
							</tr>
							<?php endforeach; ?>
					</table>
			<?php endif; ?>
			
			<!-- 
				<div class="actions">
					<ul>
						<li><?php echo $this->Html->link(__('New Circuit'), array('controller' => 'circuits', 'action' => 'add')); ?> </li>
					</ul>
				</div> 
			 -->
	</div>

