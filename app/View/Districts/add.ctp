<div class="districts form">
	<?php echo $this->Form->create('District'); ?>
	<fieldset>
		<legend><?php echo __('Crear DISTRITO'); ?></legend>
		<?php
			// echo $this->Form->input('id(10)');
			// echo $this->Form->input('name', array('label'));
			echo $this->Form->input('name', array('label' => 'Descripción'));
		?>
	</fieldset>
	<?php echo $this->Form->end(__('GUARDAR')); ?>
</div>

<div class="actions">
	<h3><?php echo __('Opciones'); ?></h3>
	<ul>
		<h4><?php echo __('DISTRITOS'); ?></h4>
		<li><?php echo $this->Html->link(__('Listar'), array('action' => 'index')); ?></li><br>

		<h4><?php echo __('CIRCUITOS'); ?></h4>
		<li><?php echo $this->Html->link(__('Listar'), array('controller' => 'circuits', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Crear'), array('controller' => 'circuits', 'action' => 'add')); ?> </li>
	</ul>
</div>
