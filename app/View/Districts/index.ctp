<div class="districts index">
	<h2><?php echo __('DISTRITOS'); ?></h2>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $this->Paginator->sort('id', '* Id *'); ?> </th>
			<th><?php echo $this->Paginator->sort('name', '* Descripción *'); ?></th>
			<th class="actions">
				<?php 
					echo __( 'Acciones' ); 
				?>
			</th>
		</tr>
		
		<?php foreach ($districts as $district): ?>
			<tr>
				<td><?php echo h($district['District']['id']); ?>&nbsp;</td>
				<!--  SE BORRA LO QUE HAY DENTRO
						<td>
							<?php echo $this->Html->link(	$district['District']['name'],
																			array('controller' => 'districts', 'action' => 'view', $district['District']['id'])	); ?> 
							&nbsp; 
						</td>
				 -->				
				<td><?php echo h($district['District']['name']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $district['District']['id'])); ?>
					<?php echo $this->Html->link(__('Modificar'), array('action' => 'edit', $district['District']['id'])); ?>

					<!-- 	ESTE CODIGO NO FUNCIONA
						<?php 
							if (!empty($district['Circuit'])) {
								echo $this->Form->postLink(	__('Borrar'), 
																						array('action' => 'delete', $district['District']['id']), 
																						null, 
																						__('¿BORRAMOS el DISTRITO < %s >?', $district['District']['name'])
																					 ); 
							}
						?> 
					 -->			
					 
					<?php 
						echo $this->Form->postLink(	__('Borrar'), 
																				array('action' => 'delete', $district['District']['id']), 
																				null, 
																				__('¿BORRAMOS el DISTRITO < %s >?', $district['District']['name'])
																			 ); 
					?> 
					
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
	
	<p>
		<?php
			echo $this->Paginator->counter(array(
				'format' => __('Pag. {:page} de {:pages}, mostrando {:current} rgtros.de {:count} , empezando en rgtro.{:start}, terminando en {:end}')
			));
		?>	
	</p>
	
	<div class="paging">
		<?php
			echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
		?>
	</div>
</div>

<div class="actions">
	<h3><?php echo __('Opciones'); ?></h3>
	<ul>
		<h4><?php echo __('DISTRITOS'); ?></h4>
		<li><?php echo $this->Html->link(__('Crear'), array('action' => 'add')); ?></li><br>
		
		<h4><?php echo __('CIRCUITOS'); ?></h4>
		<li><?php echo $this->Html->link(__('Listar'), array('controller' => 'circuits', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Crear'), array('controller' => 'circuits', 'action' => 'add')); ?> </li>
	</ul>
</div>
