<div class="estates view">
<h2><?php echo __('Estate'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($estate['Estate']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($estate['Estate']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Estate Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($estate['EstateType']['name'], array('controller' => 'estate_types', 'action' => 'view', $estate['EstateType']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Estate'), array('action' => 'edit', $estate['Estate']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Estate'), array('action' => 'delete', $estate['Estate']['id']), null, __('Are you sure you want to delete # %s?', $estate['Estate']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Estates'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Estate'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Estate Types'), array('controller' => 'estate_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Estate Type'), array('controller' => 'estate_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Congregations'), array('controller' => 'congregations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Congregation'), array('controller' => 'congregations', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Projects'); ?></h3>
	<?php if (!empty($estate['Project'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Estate Id'); ?></th>
		<th><?php echo __('Project Type Id'); ?></th>
		<th><?php echo __('Start Time'); ?></th>
		<th><?php echo __('End Time'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($estate['Project'] as $project): ?>
		<tr>
			<td><?php echo $project['id']; ?></td>
			<td><?php echo $project['name']; ?></td>
			<td><?php echo $project['code']; ?></td>
			<td><?php echo $project['estate_id']; ?></td>
			<td><?php echo $project['project_type_id']; ?></td>
			<td><?php echo $project['start_time']; ?></td>
			<td><?php echo $project['end_time']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'projects', 'action' => 'view', $project['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'projects', 'action' => 'edit', $project['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'projects', 'action' => 'delete', $project['id']), null, __('Are you sure you want to delete # %s?', $project['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Congregations'); ?></h3>
	<?php if (!empty($estate['Congregation'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Circuit Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($estate['Congregation'] as $congregation): ?>
		<tr>
			<td><?php echo $congregation['id']; ?></td>
			<td><?php echo $congregation['name']; ?></td>
			<td><?php echo $congregation['circuit_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'congregations', 'action' => 'view', $congregation['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'congregations', 'action' => 'edit', $congregation['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'congregations', 'action' => 'delete', $congregation['id']), null, __('Are you sure you want to delete # %s?', $congregation['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Congregation'), array('controller' => 'congregations', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
