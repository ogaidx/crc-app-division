<div class="docTypes form">
<?php echo $this->Form->create('DocType'); ?>
	<fieldset>
		<legend><?php echo __('Add Doc Type'); ?></legend>
	<?php
		echo $this->Form->input('int');
		echo $this->Form->input('name');
		echo $this->Form->input('fk_model');
		echo $this->Form->input('tags');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Doc Types'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Docs'), array('controller' => 'docs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Doc'), array('controller' => 'docs', 'action' => 'add')); ?> </li>
	</ul>
</div>
