<div class="docTypes view">
<h2><?php echo __('Doc Type'); ?></h2>
	<dl>
		<dt><?php echo __('Int'); ?></dt>
		<dd>
			<?php echo h($docType['DocType']['int']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($docType['DocType']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fk Model'); ?></dt>
		<dd>
			<?php echo h($docType['DocType']['fk_model']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tags'); ?></dt>
		<dd>
			<?php echo h($docType['DocType']['tags']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Doc Type'), array('action' => 'edit', $docType['DocType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Doc Type'), array('action' => 'delete', $docType['DocType']['id']), null, __('Are you sure you want to delete # %s?', $docType['DocType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Doc Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Doc Type'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Docs'), array('controller' => 'docs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Doc'), array('controller' => 'docs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Docs'); ?></h3>
	<?php if (!empty($docType['Doc'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Doc Type Id'); ?></th>
		<th><?php echo __('Fk Model'); ?></th>
		<th><?php echo __('Fk Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Public'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($docType['Doc'] as $doc): ?>
		<tr>
			<td><?php echo $doc['id']; ?></td>
			<td><?php echo $doc['name']; ?></td>
			<td><?php echo $doc['doc_type_id']; ?></td>
			<td><?php echo $doc['fk_model']; ?></td>
			<td><?php echo $doc['fk_id']; ?></td>
			<td><?php echo $doc['created']; ?></td>
			<td><?php echo $doc['public']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'docs', 'action' => 'view', $doc['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'docs', 'action' => 'edit', $doc['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'docs', 'action' => 'delete', $doc['id']), null, __('Are you sure you want to delete # %s?', $doc['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Doc'), array('controller' => 'docs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
