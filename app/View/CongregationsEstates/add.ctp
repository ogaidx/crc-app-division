<div class="congregationsEstates form">
<?php echo $this->Form->create('CongregationsEstate'); ?>
	<fieldset>
		<legend><?php echo __('Add Congregations Estate'); ?></legend>
	<?php
		echo $this->Form->input('congregation_id');
		echo $this->Form->input('estate_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Congregations Estates'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Congregations'), array('controller' => 'congregations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Congregation'), array('controller' => 'congregations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Estates'), array('controller' => 'estates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Estate'), array('controller' => 'estates', 'action' => 'add')); ?> </li>
	</ul>
</div>
