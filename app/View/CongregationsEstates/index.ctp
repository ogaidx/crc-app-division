<div class="congregationsEstates index">
	<h2><?php echo __('Congregations Estates'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('congregation_id'); ?></th>
			<th><?php echo $this->Paginator->sort('estate_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($congregationsEstates as $congregationsEstate): ?>
	<tr>
		<td><?php echo h($congregationsEstate['CongregationsEstate']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($congregationsEstate['Congregation']['name'], array('controller' => 'congregations', 'action' => 'view', $congregationsEstate['Congregation']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($congregationsEstate['Estate']['name'], array('controller' => 'estates', 'action' => 'view', $congregationsEstate['Estate']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $congregationsEstate['CongregationsEstate']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $congregationsEstate['CongregationsEstate']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $congregationsEstate['CongregationsEstate']['id']), null, __('Are you sure you want to delete # %s?', $congregationsEstate['CongregationsEstate']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Congregations Estate'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Congregations'), array('controller' => 'congregations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Congregation'), array('controller' => 'congregations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Estates'), array('controller' => 'estates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Estate'), array('controller' => 'estates', 'action' => 'add')); ?> </li>
	</ul>
</div>
