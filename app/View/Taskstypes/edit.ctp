<div class="taskstypes form">
<?php echo $this->Form->create('Taskstype'); ?>
	<fieldset>
		<legend><?php echo __('Edit Taskstype'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('start');
		echo $this->Form->input('end');
		echo $this->Form->input('limit');
		echo $this->Form->input('repeat_id');
		echo $this->Form->input('phase_id');
		echo $this->Form->input('parent_id');
		echo $this->Form->input('project_id');
		echo $this->Form->input('department_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Taskstype.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Taskstype.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Taskstypes'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Repeats'), array('controller' => 'repeats', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Repeat'), array('controller' => 'repeats', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Phases'), array('controller' => 'phases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phase'), array('controller' => 'phases', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Taskstypes'), array('controller' => 'taskstypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Parent Taskstype'), array('controller' => 'taskstypes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Departments'), array('controller' => 'departments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Department'), array('controller' => 'departments', 'action' => 'add')); ?> </li>
	</ul>
</div>
