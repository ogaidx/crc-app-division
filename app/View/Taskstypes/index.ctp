<div class="taskstypes index">
	<h2><?php echo __('Taskstypes'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('start'); ?></th>
			<th><?php echo $this->Paginator->sort('end'); ?></th>
			<th><?php echo $this->Paginator->sort('limit'); ?></th>
			<th><?php echo $this->Paginator->sort('repeat_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('phase_id'); ?></th>
			<th><?php echo $this->Paginator->sort('parent_id'); ?></th>
			<th><?php echo $this->Paginator->sort('project_id'); ?></th>
			<th><?php echo $this->Paginator->sort('department_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($taskstypes as $taskstype): ?>
	<tr>
		<td><?php echo h($taskstype['Taskstype']['id']); ?>&nbsp;</td>
		<td><?php echo h($taskstype['Taskstype']['name']); ?>&nbsp;</td>
		<td><?php echo h($taskstype['Taskstype']['start']); ?>&nbsp;</td>
		<td><?php echo h($taskstype['Taskstype']['end']); ?>&nbsp;</td>
		<td><?php echo h($taskstype['Taskstype']['limit']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($taskstype['Repeat']['name'], array('controller' => 'repeats', 'action' => 'view', $taskstype['Repeat']['id'])); ?>
		</td>
		<td><?php echo h($taskstype['Taskstype']['created']); ?>&nbsp;</td>
		<td><?php echo h($taskstype['Taskstype']['modified']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($taskstype['Phase']['name'], array('controller' => 'phases', 'action' => 'view', $taskstype['Phase']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($taskstype['ParentTaskstype']['name'], array('controller' => 'taskstypes', 'action' => 'view', $taskstype['ParentTaskstype']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($taskstype['Project']['name'], array('controller' => 'projects', 'action' => 'view', $taskstype['Project']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($taskstype['Department']['name'], array('controller' => 'departments', 'action' => 'view', $taskstype['Department']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $taskstype['Taskstype']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $taskstype['Taskstype']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $taskstype['Taskstype']['id']), null, __('Are you sure you want to delete # %s?', $taskstype['Taskstype']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Taskstype'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Repeats'), array('controller' => 'repeats', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Repeat'), array('controller' => 'repeats', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Phases'), array('controller' => 'phases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phase'), array('controller' => 'phases', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Taskstypes'), array('controller' => 'taskstypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Parent Taskstype'), array('controller' => 'taskstypes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Departments'), array('controller' => 'departments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Department'), array('controller' => 'departments', 'action' => 'add')); ?> </li>
	</ul>
</div>
