<div class="phases view">
<h2><?php echo __('Phase'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($phase['Phase']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($phase['Phase']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($phase['ProjectType']['name'], array('controller' => 'project_types', 'action' => 'view', $phase['ProjectType']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Phase'), array('action' => 'edit', $phase['Phase']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Phase'), array('action' => 'delete', $phase['Phase']['id']), null, __('Are you sure you want to delete # %s?', $phase['Phase']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Phases'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phase'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Types'), array('controller' => 'project_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Type'), array('controller' => 'project_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tasks'), array('controller' => 'tasks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Task'), array('controller' => 'tasks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Taskstypes'), array('controller' => 'taskstypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Taskstype'), array('controller' => 'taskstypes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Tasks'); ?></h3>
	<?php if (!empty($phase['Task'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Start'); ?></th>
		<th><?php echo __('End'); ?></th>
		<th><?php echo __('Limit'); ?></th>
		<th><?php echo __('Repeat Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Phase Id'); ?></th>
		<th><?php echo __('Parent Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Department Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($phase['Task'] as $task): ?>
		<tr>
			<td><?php echo $task['id']; ?></td>
			<td><?php echo $task['name']; ?></td>
			<td><?php echo $task['start']; ?></td>
			<td><?php echo $task['end']; ?></td>
			<td><?php echo $task['limit']; ?></td>
			<td><?php echo $task['repeat_id']; ?></td>
			<td><?php echo $task['created']; ?></td>
			<td><?php echo $task['modified']; ?></td>
			<td><?php echo $task['phase_id']; ?></td>
			<td><?php echo $task['parent_id']; ?></td>
			<td><?php echo $task['project_id']; ?></td>
			<td><?php echo $task['department_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'tasks', 'action' => 'view', $task['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'tasks', 'action' => 'edit', $task['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'tasks', 'action' => 'delete', $task['id']), null, __('Are you sure you want to delete # %s?', $task['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Task'), array('controller' => 'tasks', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Taskstypes'); ?></h3>
	<?php if (!empty($phase['Taskstype'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Start'); ?></th>
		<th><?php echo __('End'); ?></th>
		<th><?php echo __('Limit'); ?></th>
		<th><?php echo __('Repeat Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Phase Id'); ?></th>
		<th><?php echo __('Parent Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Department Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($phase['Taskstype'] as $taskstype): ?>
		<tr>
			<td><?php echo $taskstype['id']; ?></td>
			<td><?php echo $taskstype['name']; ?></td>
			<td><?php echo $taskstype['start']; ?></td>
			<td><?php echo $taskstype['end']; ?></td>
			<td><?php echo $taskstype['limit']; ?></td>
			<td><?php echo $taskstype['repeat_id']; ?></td>
			<td><?php echo $taskstype['created']; ?></td>
			<td><?php echo $taskstype['modified']; ?></td>
			<td><?php echo $taskstype['phase_id']; ?></td>
			<td><?php echo $taskstype['parent_id']; ?></td>
			<td><?php echo $taskstype['project_id']; ?></td>
			<td><?php echo $taskstype['department_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'taskstypes', 'action' => 'view', $taskstype['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'taskstypes', 'action' => 'edit', $taskstype['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'taskstypes', 'action' => 'delete', $taskstype['id']), null, __('Are you sure you want to delete # %s?', $taskstype['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Taskstype'), array('controller' => 'taskstypes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
