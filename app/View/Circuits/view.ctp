<div class="circuits view">
	<h2><?php echo __('CIRCUITO ... Id('. $circuit['Circuit']['id'].')'); ?></h2>
	
	<dl>
			<!-- 		LAS LINEAS DE A CONTINUACION SE BORRAN
				<dt><?php echo __('Id'); ?></dt> 
				<dd>
					<?php echo h($circuit['Circuit']['id']); ?>
					&nbsp;
				</dd>
			-->
			
			<dt><?php echo __('Descripción'); ?></dt>
			<dd>
				<?php echo h($circuit['Circuit']['name']); ?>
				&nbsp;
			</dd>
			
			<dt><?php echo __('Distrito'); ?></dt>
			<dd>
				<?php echo $this->Html->link(	$circuit['District']['name'], 
																	array('controller' => 'districts', 'action' => 'view', $circuit['District']['id'])	); ?>
				&nbsp;
			</dd>
	</dl>
</div>


<div class="actions">
	<h3><?php echo __('Opciones'); ?></h3>
	<ul>
		<h4><?php echo __('CIRCUITOS'); ?></h4>
		<li><?php echo $this->Html->link(__('Modificar'), array('action' => 'edit', $circuit['Circuit']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $circuit['Circuit']['id']), null, __('¿BORRAMOS el CIRCUITO < %s >?', $circuit['Circuit']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Listar'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Crear'), array('action' => 'add')); ?> </li><br>
		
		<h4><?php echo __('DISTRITOS'); ?></h4>
		<li><?php echo $this->Html->link(__('Listar'), array('controller' => 'districts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Crear'), array('controller' => 'districts', 'action' => 'add')); ?> </li><br>
		
		<h4><?php echo __('CONGREGACIONES'); ?></h4>
		<li><?php echo $this->Html->link(__('Listar'), array('controller' => 'congregations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Crear'), array('controller' => 'congregations', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="related">
	<br><h2><?php echo __('CONGREGACIONES relacionadas'); ?></h2>
	<?php if (!empty($circuit['Congregation'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Circuit Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($circuit['Congregation'] as $congregation): ?>
		<tr>
			<td><?php echo $congregation['id']; ?></td>
			<td><?php echo $congregation['name']; ?></td>
			<td><?php echo $congregation['circuit_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('Ver'), array('controller' => 'congregations', 'action' => 'view', $congregation['id'])); ?>
				<?php echo $this->Html->link(__('Modificar'), array('controller' => 'congregations', 'action' => 'edit', $congregation['id'])); ?>
				<?php echo $this->Form->postLink(__('Borrar'), array('controller' => 'congregations', 'action' => 'delete', $congregation['id']), null, __('¿BORRAMOS la CONGREGACION < %s >?', $congregation['name'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<!-- 
		<div class="actions">
			<ul>
				<li><?php echo $this->Html->link(__('New Congregation'), array('controller' => 'congregations', 'action' => 'add')); ?> </li>
			</ul>
		</div>
	 -->	
	 
</div>
