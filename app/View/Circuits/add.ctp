<div class="circuits form">
	<?php echo $this->Form->create('Circuit'); ?>
	<fieldset>
		<legend><?php echo __('Crear CIRCUITO'); ?></legend>
		<?php
			echo $this->Form->input('name', array('label' => 'Descripción'));
			echo $this->Form->input('district_id', array('label' => 'Distrito'));
		?>
	</fieldset>
	<?php echo $this->Form->end(__('GUARDAR')); ?>
</div>

<div class="actions">
	<h3><?php echo __('Opciones'); ?></h3>
	<ul>
		<h4><?php echo __('CIRCUITOS'); ?></h4>
		<li><?php echo $this->Html->link(__('Listar'), array('action' => 'index')); ?></li><br>
		
		<h4><?php echo __('DISTRITOS'); ?></h4>
		<li><?php echo $this->Html->link(__('Listar'), array('controller' => 'districts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Crear'), array('controller' => 'districts', 'action' => 'add')); ?> </li><br>
		
		<h4><?php echo __('CONGREGACIONES'); ?></h4>
		<li><?php echo $this->Html->link(__('Listar'), array('controller' => 'congregations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Crear'), array('controller' => 'congregations', 'action' => 'add')); ?> </li>
	</ul>
</div>
