<div class="circuits index">
	<h2><?php echo __('CIRCUITOS'); ?></h2>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $this->Paginator->sort('id', '* Id *'); ?> </th>
			<th><?php echo $this->Paginator->sort('name', '* Descripción *'); ?></th>
			<th><?php echo $this->Paginator->sort('district_id', '* Distrito *'); ?></th>
			<th class="actions">
				<?php echo __('Acciones'); ?>
			</th>
		</tr>
		
		<?php foreach ($circuits as $circuit): ?>
			<tr>
				<td><?php echo h($circuit['Circuit']['id']); ?>&nbsp;</td>

				<td><?php echo h($circuit['Circuit']['name']); ?>&nbsp;</td> 
				
				<!--  LO QUE HAY DENTRO SE BORRA
						<td>
							<?php echo $this->Html->link(	$circuit['Circuit']['name'],
																				array('controller' => 'circuits', 'action' => 'view', $circuit['Circuit']['id'])	); ?> 
							&nbsp; 
						</td>
				-->

				<td>
					<?php echo $this->Html->link(	'('.
																		$circuit['Circuit']['district_id'].
																		') '.
																		$circuit['District']['name'], 
																		array('controller' => 'districts', 'action' => 'view', $circuit['District']['id'])); ?>
				</td>
				
				<td class="actions">
					<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $circuit['Circuit']['id'])); ?> 
					<?php echo $this->Html->link(__('Modificar'), array('action' => 'edit', $circuit['Circuit']['id'])); ?>
					
					<?php echo $this->Form->postLink(	__('Borrar'), 
																					array('action' => 'delete', $circuit['Circuit']['id']), 
																					null, 
																					__('¿BORRAMOS el CIRCUITO < %s >?', $circuit['Circuit']['name'])	); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	
	</table>
	
	<p>
		<?php
			echo $this->Paginator->counter(array(
				'format' => __('Pag. {:page} de {:pages}, mostrando {:current} rgtros.de {:count} , empezando en rgtro.{:start}, terminando en {:end}')
			));
		?>	
	</p>
	
	<div class="paging">
		<?php
			echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
		?>
	</div>
</div>

<div class="actions">
	<h3><?php echo __('Opciones'); ?></h3>
	<ul>
		<h4><?php echo __('CIRCUITOS'); ?></h4>
		<li><?php echo $this->Html->link(__('Crear'), array('action' => 'add')); ?></li><br>

		<h4><?php echo __('DISTRITOS'); ?></h4>
		<li><?php echo $this->Html->link(__('Listar'), array('controller' => 'districts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Crear'), array('controller' => 'districts', 'action' => 'add')); ?> </li><br>

		<h4><?php echo __('CONGREGACIONES'); ?></h4>
		<li><?php echo $this->Html->link(__('Listar'), array('controller' => 'congregations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Crear'), array('controller' => 'congregations', 'action' => 'add')); ?> </li>
	</ul>
</div>
