<div class="estateTypes form">
<?php echo $this->Form->create('EstateType'); ?>
	<fieldset>
		<legend><?php echo __('Edit Estate Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('EstateType.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('EstateType.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Estate Types'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Estates'), array('controller' => 'estates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Estate'), array('controller' => 'estates', 'action' => 'add')); ?> </li>
	</ul>
</div>
