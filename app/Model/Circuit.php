<?php
	App::uses('AppModel', 'Model');
	
	/**  *****************************************************************************************************************************************	**
	 **  Circuit Model																																											**
	 **																																																	**
	 **  @property District $District																																						**
	 **  @property Congregation $Congregation																																**
	 **  *****************************************************************************************************************************************	**/
	class Circuit extends AppModel {

		/**  **************************************************************************************************************************************	**
		 **  Validation rules																																										**
		 **																																																**
		 **  @var array																																											**
		 **  **************************************************************************************************************************************	**/
		public $validate = array(
				'name' => array(
					'rule' => 'notEmpty',
				),
				'district_id' => array(
					'rule' => 'notEmpty',
				),
			/*	
				'id(10)' => array(
					'numeric' => array(
						'rule' => array('numeric'),
						'message' => 'INTRODUZCA correcto el código del CIRCUITO',
						//'allowEmpty' => false,
						//'required' => false,
						//'last' => false, // Stop validation after this rule
						//'on' => 'create', // Limit validation to 'create' or 'update' operations
					),
				),
			*/
		);

		/**  **************************************************************************************************************************************	**
		 **  The Associations below have been created with all possible keys, those that are not needed can be removed		**
		 **  **************************************************************************************************************************************	**
		 **  belongsTo associations																																						**
		 **																																																**
		 **  @var array																																											**
		 **  **************************************************************************************************************************************	**/
		public $belongsTo = array(
			'District' => array(
				'className' => 'District',
				'foreignKey' => 'district_id',
				'conditions' => '',
				'fields' => '',
				'order' => ''
			)
		);

		/**  **************************************************************************************************************************************	**
		 **  hasMany associations																																							**
		 **																																																**
		 **  @var array																																											**
		 **  **************************************************************************************************************************************	**/
		public $hasMany = array(
			'Congregation' => array(
				'className' => 'Congregation',
				'foreignKey' => 'circuit_id',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
				'order' => '',
				'limit' => '',
				'offset' => '',
				'exclusive' => '',
				'finderQuery' => '',
				'counterQuery' => ''
			)
		);

	}
?>