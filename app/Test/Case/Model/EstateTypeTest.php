<?php
App::uses('EstateType', 'Model');

/**
 * EstateType Test Case
 *
 */
class EstateTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.estate_type',
		'app.estate'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->EstateType = ClassRegistry::init('EstateType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->EstateType);

		parent::tearDown();
	}

}
