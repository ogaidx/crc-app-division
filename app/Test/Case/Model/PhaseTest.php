<?php
App::uses('Phase', 'Model');

/**
 * Phase Test Case
 *
 */
class PhaseTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.phase',
		'app.project_type',
		'app.task',
		'app.taskstype'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Phase = ClassRegistry::init('Phase');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Phase);

		parent::tearDown();
	}

}
