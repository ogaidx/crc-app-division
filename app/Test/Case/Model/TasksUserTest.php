<?php
App::uses('TasksUser', 'Model');

/**
 * TasksUser Test Case
 *
 */
class TasksUserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tasks_user',
		'app.user',
		'app.task',
		'app.repeat',
		'app.taskstype',
		'app.phase',
		'app.project_type',
		'app.project',
		'app.estate',
		'app.estate_type',
		'app.congregation',
		'app.circuit',
		'app.district',
		'app.congregations_estate',
		'app.assistance',
		'app.order',
		'app.department',
		'app.line'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TasksUser = ClassRegistry::init('TasksUser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TasksUser);

		parent::tearDown();
	}

}
