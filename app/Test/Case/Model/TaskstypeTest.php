<?php
App::uses('Taskstype', 'Model');

/**
 * Taskstype Test Case
 *
 */
class TaskstypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.taskstype',
		'app.repeat',
		'app.task',
		'app.phase',
		'app.project_type',
		'app.project',
		'app.estate',
		'app.estate_type',
		'app.congregation',
		'app.circuit',
		'app.district',
		'app.congregations_estate',
		'app.assistance',
		'app.user',
		'app.order',
		'app.department',
		'app.line',
		'app.tasks_user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Taskstype = ClassRegistry::init('Taskstype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Taskstype);

		parent::tearDown();
	}

}
