<?php
App::uses('Assistance', 'Model');

/**
 * Assistance Test Case
 *
 */
class AssistanceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.assistance',
		'app.user',
		'app.project'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Assistance = ClassRegistry::init('Assistance');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Assistance);

		parent::tearDown();
	}

}
