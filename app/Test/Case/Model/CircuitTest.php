<?php
App::uses('Circuit', 'Model');

/**
 * Circuit Test Case
 *
 */
class CircuitTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.circuit',
		'app.district',
		'app.congregation'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Circuit = ClassRegistry::init('Circuit');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Circuit);

		parent::tearDown();
	}

}
