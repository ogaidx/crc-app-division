<?php
App::uses('AppController', 'Controller');
/**
 * Congregations Controller
 *
 * @property Congregation $Congregation
 */
class CongregationsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Congregation->recursive = 0;
		$this->set('congregations', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Congregation->exists($id)) {
			throw new NotFoundException(__('Invalid congregation'));
		}
		$options = array('conditions' => array('Congregation.' . $this->Congregation->primaryKey => $id));
		$this->set('congregation', $this->Congregation->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Congregation->create();
			if ($this->Congregation->save($this->request->data)) {
				$this->Session->setFlash(__('The congregation has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The congregation could not be saved. Please, try again.'));
			}
		}
		$circuits = $this->Congregation->Circuit->find('list');
		$estates = $this->Congregation->Estate->find('list');
		$this->set(compact('circuits', 'estates'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Congregation->exists($id)) {
			throw new NotFoundException(__('Invalid congregation'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Congregation->save($this->request->data)) {
				$this->Session->setFlash(__('The congregation has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The congregation could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Congregation.' . $this->Congregation->primaryKey => $id));
			$this->request->data = $this->Congregation->find('first', $options);
		}
		$circuits = $this->Congregation->Circuit->find('list');
		$estates = $this->Congregation->Estate->find('list');
		$this->set(compact('circuits', 'estates'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Congregation->id = $id;
		if (!$this->Congregation->exists()) {
			throw new NotFoundException(__('Invalid congregation'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Congregation->delete()) {
			$this->Session->setFlash(__('Congregation deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Congregation was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
