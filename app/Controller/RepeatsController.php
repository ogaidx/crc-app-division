<?php
App::uses('AppController', 'Controller');
/**
 * Repeats Controller
 *
 * @property Repeat $Repeat
 */
class RepeatsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Repeat->recursive = 0;
		$this->set('repeats', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Repeat->exists($id)) {
			throw new NotFoundException(__('Invalid repeat'));
		}
		$options = array('conditions' => array('Repeat.' . $this->Repeat->primaryKey => $id));
		$this->set('repeat', $this->Repeat->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Repeat->create();
			if ($this->Repeat->save($this->request->data)) {
				$this->Session->setFlash(__('The repeat has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The repeat could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Repeat->exists($id)) {
			throw new NotFoundException(__('Invalid repeat'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Repeat->save($this->request->data)) {
				$this->Session->setFlash(__('The repeat has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The repeat could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Repeat.' . $this->Repeat->primaryKey => $id));
			$this->request->data = $this->Repeat->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Repeat->id = $id;
		if (!$this->Repeat->exists()) {
			throw new NotFoundException(__('Invalid repeat'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Repeat->delete()) {
			$this->Session->setFlash(__('Repeat deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Repeat was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
