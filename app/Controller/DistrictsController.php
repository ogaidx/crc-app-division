<?php
	App::uses('AppController', 'Controller');
		
	/** ******************************************************************************************************************************************	**
	** Districts Controller																																										**
	**																																																		**
	** @property District $District																																						**
	 *********************************************************************************************************************************************	**/
	class DistrictsController extends AppController {

		public $paginate = array(
			'limit' => 25
		);

		/** ***************************************************************************************************************************************	**
		 ** index METODO 																																									**
		 **																																																**
		 ** @return void                                                                                                                                                             				**
		 ******************************************************************************************************************************************	**/
		public function index() {
			$this->District->recursive = 0;
			$this->set('districts', $this->paginate());
		}

		/** ***************************************************************************************************************************************	**
		 ** view method																																											**
		 **																																																**
		 ** @throws NotFoundException																																				**
		 ** @param string $id																																								**
		 ** @return void																																											**
		 ******************************************************************************************************************************************	**/
		public function view($id = null) {
			if (!$this->District->exists($id)) {
				throw new NotFoundException(__('No existe este DISTRITO'));
			}
			$options = array('conditions' => array('District.' . $this->District->primaryKey => $id));
			$this->set('district', $this->District->find('first', $options));
		}

		/** ***************************************************************************************************************************************	**
		 * add method																																											**
		 *																																																	**
		 * @return void																																											**
		 ******************************************************************************************************************************************	**/
		public function add() {
			if ($this->request->is('post')) {
				$this->District->create();
				
				if ($this->District->save($this->request->data)) {
					$this->Session->setFlash(__('DISTRITO guardado con éxito'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('DISTRITO no guardado. Inténtelo otra vez.'));
				}
			}
		}

		/** ***************************************************************************************************************************************	**
		 ** edit method																																											**
		 **																																																**
		 ** @throws NotFoundException																																				**
		 ** @param string $id																																								**
		 ** @return void																																											**
		 ******************************************************************************************************************************************	**/
		public function edit($id = null) {
			if (!$this->District->exists($id)) {
				throw new NotFoundException(__('DISTRITO inválido'));
			}
			
			if ($this->request->is('post') || $this->request->is('put')) {
				if ($this->District->save($this->request->data)) {
					$this->Session->setFlash(__('DISTRITO guardado con éxito'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('DISTRITO no guardado. Inténtelo otra vez.'));
				}
			} else {
				$options = array('conditions' => array('District.' . $this->District->primaryKey => $id));
				$this->request->data = $this->District->find('first', $options);
			}
		}

		/** ***************************************************************************************************************************************	**
		 ** delete method																																										**
		 **																																																**
		 ** @throws NotFoundException																																				**
		 ** @param string $id																																								**
		 ** @return void																																											**
		 ******************************************************************************************************************************************	**/
		public function delete($id = null) {
			$this->District->id = $id;
			if (!$this->District->exists()) {
				throw new NotFoundException(__('DISTRITO inválido'));
			}
			
			$this->request->onlyAllow('post', 'delete');
			if ($this->District->delete()) {
				$this->Session->setFlash(__('DISTRITO borrado'));
				$this->redirect(array('action' => 'index'));
			}
			
			$this->Session->setFlash(__('El DISTRITO no pudo ser borrado'));
			$this->redirect(array('action' => 'index'));
		}
	}
?>