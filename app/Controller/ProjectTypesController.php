<?php
App::uses('AppController', 'Controller');
/**
 * ProjectTypes Controller
 *
 * @property ProjectType $ProjectType
 */
class ProjectTypesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProjectType->recursive = 0;
		$this->set('projectTypes', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProjectType->exists($id)) {
			throw new NotFoundException(__('Invalid project type'));
		}
		$options = array('conditions' => array('ProjectType.' . $this->ProjectType->primaryKey => $id));
		$this->set('projectType', $this->ProjectType->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProjectType->create();
			if ($this->ProjectType->save($this->request->data)) {
				$this->Session->setFlash(__('The project type has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project type could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProjectType->exists($id)) {
			throw new NotFoundException(__('Invalid project type'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ProjectType->save($this->request->data)) {
				$this->Session->setFlash(__('The project type has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project type could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProjectType.' . $this->ProjectType->primaryKey => $id));
			$this->request->data = $this->ProjectType->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProjectType->id = $id;
		if (!$this->ProjectType->exists()) {
			throw new NotFoundException(__('Invalid project type'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ProjectType->delete()) {
			$this->Session->setFlash(__('Project type deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Project type was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
