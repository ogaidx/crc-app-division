<?php
	App::uses('AppController', 'Controller');
	
	/** ******************************************************************************************************************************************	**
	** Circuits Controller																																										**
	**																																																		**
	** @property Circuit $Circuit																																						**
	 *********************************************************************************************************************************************	**/
	class CircuitsController extends AppController {

		public $paginate = array(
			'limit' => 25
		);

		/** ***************************************************************************************************************************************	**
		 ** index METODO 																																									**
		 **																																																**
		 ** @return void                                                                                                                                                             				**
		 ******************************************************************************************************************************************	**/
		public function index() {
			$this->Circuit->recursive = 0;
			$this->set('circuits', $this->paginate());
		}

	
		/** ***************************************************************************************************************************************	**
		 ** view method																																											**
		 **																																																**
		 ** @throws NotFoundException																																				**
		 ** @param string $id																																								**
		 ** @return void																																											**
		 ******************************************************************************************************************************************	**/
		public function view($id = null) {
			if (!$this->Circuit->exists($id)) {
				throw new NotFoundException(__('No existe este CIRCUITO'));
			}
			$options = array('conditions' => array('Circuit.' . $this->Circuit->primaryKey => $id));
			$this->set('circuit', $this->Circuit->find('first', $options));
		}

		/** ***************************************************************************************************************************************	**
		 * add method																																											**
		 *																																																	**
		 * @return void																																											**
		 ******************************************************************************************************************************************	**/
		public function add() {
			if ($this->request->is('post')) {
				$this->Circuit->create();
				
				if ($this->Circuit->save($this->request->data)) {
					$this->Session->setFlash(__('CIRCUITO guardado con éxito'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('CIRCUITO no guardado. Inténtelo otra vez.'));
				}
			}
			
			$districts = $this->Circuit->District->find('list');
			$this->set(compact('districts'));
		}

		/** ***************************************************************************************************************************************	**
		 ** edit method																																											**
		 **																																																**
		 ** @throws NotFoundException																																				**
		 ** @param string $id																																								**
		 ** @return void																																											**
		 ******************************************************************************************************************************************	**/
		public function edit($id = null) {
			if (!$this->Circuit->exists($id)) {
				throw new NotFoundException(__('CIRCUITO incorrecto'));
			}
			
			if ($this->request->is('post') || $this->request->is('put')) {
				if ($this->Circuit->save($this->request->data)) {
					$this->Session->setFlash(__('CIRCUITO guardado con éxito'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('CIRCUITO no guardado. Inténtelo otra vez.'));
				}
			} else {
				$options = array('conditions' => array('Circuit.' . $this->Circuit->primaryKey => $id));
				$this->request->data = $this->Circuit->find('first', $options);
			}
			
			$districts = $this->Circuit->District->find('list');
			$this->set(compact('districts'));
		}

		/** ***************************************************************************************************************************************	**
		 ** delete method																																										**
		 **																																																**
		 ** @throws NotFoundException																																				**
		 ** @param string $id																																								**
		 ** @return void																																											**
		 ******************************************************************************************************************************************	**/
		public function delete($id = null) {
			$this->Circuit->id = $id;
			if (!$this->Circuit->exists()) {
				throw new NotFoundException(__('CIRCUITO incorrecto'));
			}
			
			$this->request->onlyAllow('post', 'delete');
			if ($this->Circuit->delete()) {
				$this->Session->setFlash(__('CIRCUITO borrado'));
				$this->redirect(array('action' => 'index'));
			}
			
			$this->Session->setFlash(__('El CIRCUITO no pudo ser borrado'));
			$this->redirect(array('action' => 'index'));
		}
	}
?>