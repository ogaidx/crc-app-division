Aplicación para el CRC
======================

Aplicación de gestión integral para el CRC.

Advertencias
------------

No consignar cambios en `app/Config/database.php`. Para que se haga de forma
automática, tras clonar el repositorio, ejecuta:

```
$ git update-index --assume-unchanged app/Config/database.php
```

### Uso de mod_userdir

Tendrás que descomentar y configurar el RewriteBase de los ficheros
`.htaccess` y `app/webroot/.htaccess`.
